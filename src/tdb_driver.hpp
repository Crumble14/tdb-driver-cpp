#ifndef TDB_DRIVER
# define TDB_DRIVER

# include<string>
# include<unordered_map>

# include<socket.hpp>

# define DEFAULT_PORT	8123

namespace TDb
{
	using namespace std;
	using namespace Network;

	typedef uint32_t index_t;

	enum Type : uint8_t
	{
		INT_T = 0,
		FLOAT_T,
		BOOL_T,
		STRING_T,
		CLOB_T
	};

	enum RequestType : uint8_t
	{
		LOGIN = 0,
		GET,
		SET,
		INSERT,
		REMOVE,
		CLEAR,
		PROCESS
	};

	struct Column
	{
		string name;
		Type type;
		uint8_t size;

		inline Column(const string& name, const Type type, const uint8_t size)
			: name{name}, type{type}, size{size}
		{}

		inline Column(const string&& name, const Type type, const uint8_t size)
			: Column(name, type, size)
		{}
	};

	struct Row
	{
		index_t index;
		vector<void*> values;

		Row() = default;

		inline Row(const index_t index, const vector<void*>& values)
			: index{index}, values{values}
		{}

		inline ~Row()
		{
			for(const auto& v : values) free(v);
		}

		template<typename T>
		inline const T& get_value(const size_t column) const
		{
			return *reinterpret_cast<const T*>(values[column]);
		}
	};

	struct Line
	{
		vector<Column> columns;
		Row row;

		inline Line(const vector<Column>& columns, const Row& row)
			: columns{columns}, row{row}
		{}

		inline operator Row()
		{
			return row;
		}

		size_t get_column_index(const string& column) const;

		template<typename T>
		inline const T& get_value(const size_t column) const
		{
			return row.get_value<T>(column);
		}

		template<typename T>
		inline const T& get_value(const string column) const
		{
			return row.get_value<T>(get_column_index(column));
		}
	};

	struct Value
	{
		string column;

		uint32_t size;
		void* data;

		template<typename T>
		inline Value(const string column, const T value)
			: column{column}, size{sizeof(value)}, data{malloc(size)}
		{
			memcpy(data, &value, size);
		}

		inline ~Value()
		{
			free(data);
		}
	};

	template<>
	inline Value::Value(const string column, const string value)
		: column{column}, size(value.size()), data{malloc(size)}
	{
		memcpy(data, &value, size);
	}

	template<>
	inline Value::Value(const string column, const char* value)
		: Value(column, string(value))
	{}

	template<typename T>
	inline void write_value(const int sock, const T&& value)
	{
		sock_write(sock, (const char*)&value, sizeof(T));
	}

	class Request
	{
		public:
			inline Request(const int sock, const string& table)
				: sock{sock}, table{table}
			{}

			inline const string& get_table() const
			{
				return table;
			}

			inline Request& select(const string condition)
			{
				conditions.push_back(condition);
				return *this;
			}

			string get_condition() const;

			void get(const function<bool(const Line&)> handle) const;
			// TODO Set
			index_t insert(const initializer_list<Value> values) const;
			void remove() const;
			void clear() const;

		private:
			const int sock;
			const string table;

			vector<string> conditions;
	};

	class Connection
	{
		public:
			inline Connection(const string& host,
				const port_t port = DEFAULT_PORT)
				: host{host}, port{port}
			{
				connect();
			}

			inline Connection(const string&& host,
				const port_t port = DEFAULT_PORT)
				: Connection(host, port)
			{}

			inline const string& get_host() const
			{
				return host;
			}

			inline port_t get_port() const
			{
				return port;
			}

			inline void set_auto_reconnect(const bool auto_reconnect)
			{
				this->auto_reconnect = auto_reconnect;
			}

			bool login(const string& username, const string& password);

			inline bool login(const string&& username, const string&& password)
			{
				return login(username, password);
			}

			inline bool is_logged() const
			{
				return logged;
			}

			inline const string* get_user() const
			{
				return (is_logged() ? &username : nullptr);
			}

			Request table(const string& name);

			inline Request table(const string&& name)
			{
				return table(name);
			}

			// TODO Process

		private:
			const string host;
			const port_t port;

			bool auto_reconnect = true;

			int sock;

			bool logged = false;
			string username, password;

			void connect();
			void reconnect();
	};
}

#endif
