#include "tdb_driver.hpp"

using namespace TDb;

static void read_count(const int sock, char* buffer, const size_t size)
{
	for(size_t i = 0; i < size;) {
		i += sock_read(sock, buffer + i, size - i);
	}
}

static bool is_null(const vector<Column>& columns, const Row& row)
{
	if(row.index != 0) return false;

	for(size_t i = 0; i < columns.size(); ++i) {
		const auto& size = columns[i].size;

		for(size_t j = 0; j < size; ++j) {
			if(*((char*) row.values[i] + j) != 0) return false;
		}
	}

	return true;
}

string Request::get_condition() const
{
	string str;

	for(size_t i = 0; i < conditions.size(); ++i) {
		str += '(';
		str += conditions[i];
		str += ')';

		if(i < conditions.size() - 1) {
			str += " & ";
		}
	}

	return str;
}

void Request::get(const function<bool(const Line&)> handle) const
{
	write_value(sock, GET);
	sock_write(sock, table.c_str(), table.size() + 1);

	const auto condition = get_condition();
	sock_write(sock, condition.c_str(), condition.size() + 1);

	char type;
	sock_read(sock, &type, sizeof(type));

	if(type != 1) {
		// TODO Error
		return;
	}

	uint32_t head_size;
	read_count(sock, (char*) &head_size, sizeof(head_size));

	vector<Column> columns;

	for(size_t i = 0; i < head_size;) {
		string name;
		char c;

		while(true) {
			read_count(sock, &c, 1);
			if(c == '\0') break;

			name += c;
		}

		Type type;
		read_count(sock, (char*) &type, sizeof(type));
		i += sizeof(type);

		uint8_t size;
		read_count(sock, (char*) &size, sizeof(size));
		i += sizeof(size);

		columns.emplace_back(name, type, size);
	}

	while(true) {
		Row row;
		read_count(sock, (char*) &row.index, sizeof(row.index));

		for(size_t i = 0; i < columns.size(); ++i) {
			const auto& size = columns[i].size;

			// TODO Handle CLOBs
			void* val;
			if(!(val = malloc(size))) throw bad_alloc();

			read_count(sock, (char*) val, size);
			row.values.push_back(val);
		}

		if(is_null(columns, row)) break;
		if(!handle(Line(columns, row))) break;
	}
}

index_t Request::insert(const initializer_list<Value> values) const
{
	write_value(sock, INSERT);

	for(auto i = values.begin(); i != values.end(); ++i) {
		const auto& col = (*i).column;
		const auto& size = (*i).size;

		sock_write(sock, col.c_str(), col.size() + 1);
		sock_write(sock, (const char*) &size, sizeof(size));
		sock_write(sock, (const char*) (*i).data, size);
	}

	// TODO Check error
	index_t index;
	sock_read(sock, (char*) &index, sizeof(index));

	return index;
}

void Request::remove() const
{
	write_value(sock, REMOVE);
	sock_write(sock, table.c_str(), table.size() + 1);

	const auto condition = get_condition();
	sock_write(sock, condition.c_str(), condition.size() + 1);

	// TODO Check error
}

void Request::clear() const
{
	write_value(sock, CLEAR);
	sock_write(sock, table.c_str(), table.size() + 1);

	// TODO Check error
}
