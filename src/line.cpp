#include "tdb_driver.hpp"

using namespace TDb;

size_t Line::get_column_index(const string& column) const
{
	for(size_t i = 0; i < columns.size(); ++i) {
		if(columns[i].name == column) return i;
	}

	throw invalid_argument("Column not found!");
}
