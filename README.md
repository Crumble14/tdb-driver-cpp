# TDb Driver C++

This is the C++ version of the TDb Driver.
The TDb Driver allows perform requests on a TDb database from a program.



## Example

```cpp
TDb::Connection connection("localhost"); // Connection to `localhost`

if(!connection.login("root", "123")) { // Trying to login
	// Login failed
	exit(-1);
}

connection.request("GET test ?foo > 1"); // Performing a request (getting all the rows from the table `test` which have a value greater than `1` for the column `foo`)

TDb::Data data;
connection.fetch(data); // Fetching the result

const auto foo_col = data.get_column_index("foo"); // Getting the `foo` column

for(const auto& r : data.rows) { // Iterating over all the returned lines
	cout << r.get_value<int>(foo_col) << '\n'; // Printing the value of the `foo` column
}
```
